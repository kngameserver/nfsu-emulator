# Need for Speed Underground "Masterserver Emulator" Docker Image!

Here you get a small Docker Image of the Masterserver Emulator for Need for Speed Underground. So you can play again Multiplayer Matches and Ranked since EA shutdown the official Servers.


## 📦 What you get

 - Minimal Alpine Based Image
 - Based on the [nfsuserver](https://github.com/HarpyWar/nfsuserver/) (Props to: HarpyWar & 3 PriedeZ)
 - It support only Need for Speed: Underground! NOT Underground 2!
 - Download latest Emulator Release!

## 🚀 How to start
For a fast start simply 

     docker create \
      --name nfsu-emulator \
      -p 10900-10901:10900-10901 \
      -p 10800:10800 \
      -p 10980:10980 \
      -p 10980:10980/udp \
      -v <your/path/to/config>:/nfsuserver \
    keinnerd/nfsu-emulator:latest

## 🔧 Config

Edit 'nfsu.conf' and 'news.txt' to suit your needs.
After first start 'server.log' should be created. After first user login a database 'rusers.dat' with users should be created.

## 📝 ToDo

## 👐 Contribution

Feel free to fork and make pull requests.