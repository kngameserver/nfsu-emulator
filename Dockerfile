FROM alpine:latest

LABEL maintainer="fistwho@keinnerd.net"
ENV Version="1.0.4"

RUN apk add --no-cache wget
RUN wget https://github.com/HarpyWar/nfsuserver/releases/download/server104/nfsuserver.${Version}.linux.static.gz \
    && mkdir /nfsuserver \
    && tar -xvf nfsuserver.${Version}.linux.static.gz -C /nfsuserver

# Server "main" folder volume
VOLUME [ "/nfsuserver" ]

# Set the server dir
WORKDIR /nfsuserver/

# Exposed server ports
EXPOSE 10900/tcp 10901/tcp 10980/tcp 10800/tcp 10800/udp

# Launch server at container startup
ENTRYPOINT ["./nfsuserver"]
